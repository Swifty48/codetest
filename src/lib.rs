//! codetest a code test cli tool
//!
//! Reads in Json data from a file where each line is a valid json blob.
//! It records the number of messages with the "type" key and keeps the
//! total bytes size of all messages of the same type
//!
//! # Objectives
//! To demonstrate an appliction that is:
//! - Easy to use
//! - Fast
//! - High Quality Code
//! - Error Handling
//!
//! Unit tests omitted as requested

use log::{info, warn};
use serde_json::Value;
use std::collections::HashMap;
use std::error::Error;
use std::fmt;
use std::mem;

pub mod reader;

/// Error for when serde_json::Value can't be converted to a string
#[derive(Debug)]
pub struct ValueConversionError {
    details: String,
}

impl ValueConversionError {
    pub fn new(msg: &str) -> ValueConversionError {
        ValueConversionError {
            details: msg.to_string(),
        }
    }
}

impl fmt::Display for ValueConversionError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.details)
    }
}

impl Error for ValueConversionError {
    fn description(&self) -> &str {
        &self.details
    }
}

/// # Examples
///
/// ```
/// use serde_json::Value;
/// use codetest::convert_value_to_string;
///
/// let val = Value::from(6);
/// assert_eq!(convert_value_to_string(&val).unwrap(), "6")
/// ```
pub fn convert_value_to_string(val: &Value) -> Result<String, ValueConversionError> {
    match val {
        Value::Null => Err(ValueConversionError::new("Can't convert Null to String")),
        Value::Bool(b) => Ok(b.to_string()),
        Value::Number(n) => Ok(n.to_string()),
        Value::String(x) => Ok(x.to_string()),
        Value::Array(_) => Err(ValueConversionError::new(
            "Message with an array value for the type key present: Can't convert Array to String",
        )),
        Value::Object(_) => Err(ValueConversionError::new(
            "Message with an object value for the type key present: Can't convert Object to String",
        )),
    }
}

/// Summary data for each message "type" value
#[derive(Debug)]
pub struct TypeSummary {
    /// The number of messages of this type that have been processed
    pub count: i32,
    /// The total number of bytes of all messages of this type combined
    pub total_bytes: usize,
}

/// Reads in a file and returns a map with summary data for each message type
///
/// Error messages are propgated up to be handled by functions user - in this case main
///
/// # Arguments
///
/// * `file_path` - The path to the file containing the json messages
pub fn process_file(file_path: &String) -> Result<HashMap<String, TypeSummary>, Box<dyn Error>> {
    let mut summary: HashMap<String, TypeSummary> = HashMap::new();

    for line in reader::BufReader::open(file_path, 2048)? {
        match line {
            Ok(msg) => process_message(msg.as_str(), &mut summary)?,
            _ => (),
        }
    }
    Ok(summary)
}

/// Processes a single message - a single line from the test file
///
/// # Arguments
///
/// * `msg` - The raw json message
/// * `summary` - The Summary object for holding the stats
pub fn process_message(
    msg: &str,
    summary: &mut HashMap<String, TypeSummary>,
) -> Result<(), Box<dyn Error>> {
    let byte_size = msg.len() * mem::size_of::<char>();
    let json_data: HashMap<String, Value> = serde_json::from_str(msg)?;
    match json_data.get("type") {
        Some(x) => {
            //
            let type_string = match convert_value_to_string(x) {
                Ok(x) => x,
                Err(e) => {
                    // I fell it is better to skip and print the messag that there is an
                    // issue with processing rather than crashing the program
                    // Note: The warning message includes the raw json message to aid in debugging
                    // and checking code running as expected
                    warn!("Skipping Message as an Error was encountered whilst processing message. {}, {:#?}", e, msg);
                    return Ok(());
                }
            };
            match summary.get_mut(&type_string) {
                Some(t) => {
                    t.count += 1;
                    t.total_bytes += byte_size;
                }
                None => {
                    summary.insert(
                        type_string,
                        TypeSummary {
                            count: 1,
                            total_bytes: byte_size,
                        },
                    );
                }
            }
            Ok(())
        }
        None => {
            // Problem did not specify what to do with messages with no type
            // If they are expected I would not log due to the performance hit
            // If they are not expected logging makes sense to undestand why the are occuring
            // warn!("Message had no type in it! {:#?}", msg);
            Ok(())
        }
    }
}

/// Prints a summary table to the log
pub fn log_table(summaries: HashMap<String, TypeSummary>) {
    info!("{0: <30}| {1: <10}| {2: <10}", "Type", "Count", "Byte Size");
    info!("{:-<53}", "-");
    for (key, value) in summaries {
        info!(
            "{0: <30}| {1: <10}| {2: <10}",
            key, value.count, value.total_bytes
        )
    }
}
