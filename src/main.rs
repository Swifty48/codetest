use env_logger::Env;
use log::{error, info};
use std::env;

use codetest::{log_table, process_file};

/// codetest a code test cli tool
///
/// Reads in Json data from a file where each line is a valid json blob.
/// It records the number of messages with the "type" key and keeps the
/// total bytes size of all messages of the same type
///
/// # Objectives
/// To demonstrate an appliction that is:
/// - Easy to use
/// - Fast
/// - High Quality Code
/// - Error Handling
///
/// Unit tests omitted as requested
fn main() {
    // Basic logging setup using defaults but setting log level as info
    let env = Env::default().filter_or("CLI_LOG", "info");
    env_logger::init_from_env(env);

    // Added to give context to the log messages, isn't needed
    info!("Code Test By Craig Swift (craigswift13@gmail.com)");

    // Basic reading in of application args - keeping it simple, I would use clap for a more advanced cli tool
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        // Gracefully logging error and exiting rather than using panic
        error!(
            "Program requires only 1 argument ./codetest <path>. {:} args given.",
            args.len() - 1
        );
        std::process::exit(1);
    }
    let file_path = &args[1];

    // Gracefully error propogation and logging
    match process_file(file_path) {
        Ok(x) => log_table(x),
        Err(e) => {
            error!("{}", e);
            std::process::exit(1);
        }
    }
}
