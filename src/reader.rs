use std::fs::File;
use std::io::{self, prelude::*};
use std::rc::Rc;

/// A BufReader which reuses a buffer which has an Iterator for reading each line
///
/// I wanted to have the cleaness of using an iterator but also the speed of pre-allocating a buffer.
/// As given we do not need to hold onto the data once we have extracted the type and the message size 
/// it is a nice optimization. However, I can't claim to have come up with this method of getting around the 
/// Borrowed Iterators problem myself!
pub struct BufReader {
    reader: io::BufReader<File>,
    buf: Rc<String>,
    capacity: usize,
}

impl BufReader {
    /// Opens a new file and returns a BufReader for the file
    ///
    /// # Example
    ///
    /// ```
    /// use codetest::reader;
    /// let file_reader = reader::BufReader::open(&String::from("sample.data"), 2048).unwrap();
    /// ```
    pub fn open(path: &String, capacity: usize) -> io::Result<BufReader> {
        let file = File::open(path)?;

        let reader = io::BufReader::new(file);
        let buf = BufReader::new_buf(capacity);

        Ok(BufReader {
            reader,
            buf,
            capacity,
        })
    }

    fn new_buf(capacity: usize) -> Rc<String> {
        Rc::new(String::with_capacity(capacity))
    }
}

impl Iterator for BufReader {
    type Item = io::Result<Rc<String>>;

    /// Next iterator
    ///
    /// Clears buffer then reads in the next line
    /// Creates a new buffer if a buffer hasn't already been created
    fn next(&mut self) -> Option<Self::Item> {
        let buf = match Rc::get_mut(&mut self.buf) {
            Some(buf) => {
                buf.clear();
                buf
            }
            None => {
                self.buf = BufReader::new_buf(self.capacity);
                Rc::make_mut(&mut self.buf)
            }
        };

        self.reader
            .read_line(buf)
            .map(|u| {
                if u == 0 {
                    // Last line in file is read as 0
                    None
                } else {
                    Some(Rc::clone(&self.buf))
                }
            })
            .transpose() // Traspose result from Result<Option<>> to Option<Result<>>
    }
}
