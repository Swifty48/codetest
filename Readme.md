# Code Test
CLI tool to analyse logs

Spec: Write a CLI tool in Rust which reads a file where each line is an arbitrary json
object that includes a field called type. It should output a table containing the number
of objects with each type, and the total byte size of all the messages with each type.

*While Rust is our preferred language, if you do not have experience using it we will
also accept solutions in Kotlin or Java.

• You may use external libraries

Score based on:
• how easy to use it is
• how fast it is
• code quality
• error handling
• Unit tests will not be scored and are not necessary

Example input:
```
{"type":"B","foo":"bar","items":["one","two"]}
{"type": "A","foo": 4.0 }
{"type": "B","bar": "abcd"}
```

## Sample.data

File with the above sample data plus some additional examples where type is not a string
It includes an example where type is an array and can't be processed

## Running 

Quick way to build and run the application using the included sample data

`cargo run -- sample.data`

More invovled way:

```
cargo build --release
./target/release/codetest sample.data
```

For docs:
```
cargo doc
firefox target/doc/codetest/index.html 
```

For tests - no unit tests as requested. 
However, I do have a couple of examples of documentation example which run as tests
`cargo test`
